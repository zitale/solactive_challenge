import pytest
import requests


url = 'http://172.21.0.3:5000/'

def test_app_is_alive():
    """
    test if the app is alive
    :return:
    """
    response = requests.get(url)
    assert response.status_code == 200


def test_add_product():
    """
    test add a new product
    :return:
    """
    headers = {'Content-type': 'application/json'}
    response = requests.post(url+'product/',
                      json={"name": "Test 1",
                            "business": "Equity",
                            "currency": "USD",
                            "attributes": {"calculation.interval": 10, "calculation.isPublicised": True},
                            "asOf": 1616108400},
                      headers=headers)
    id2del = response.json()['id']
    requests.delete(url + 'del_product/'+str(id2del))
    assert response.status_code == 201



def test_get_product():
    """
    test the simple get product
    :return:
    """
    headers = {'Content-type': 'application/json'}
    post_response = requests.post(url+'product/',
                      json={"name": "Test 1",
                            "business": "Equity",
                            "currency": "USD",
                            "attributes": {"calculation.interval": 10, "calculation.isPublicised": True},
                            "asOf": 1616108400},
                      headers=headers)
    id = post_response.json()['id']
    response = requests.get(url +'product/'+str(id))
    requests.delete(url + 'del_product/'+str(id))
    assert response.status_code == 200


def test_get_calcatt():
    """
    get calcatt of p products with specified attributes
    :argument:
     - 'name'
     - 'business'
     - 'currency'
     - 'timestamp'
    :return:
     -  dict {id:calcatt}
    """
    response = requests.get('http://172.21.0.3:5000/calcatt?currency=USD&business=Equity')
    assert response.status_code == 200


def test_update_product():
    """
    modify a product and adding a record to changes to keep track
    :return:
    """
    headers = {'Content-type': 'application/json'}
    post_response = requests.post(url+'product/',
                      json={"name": "Test 1",
                            "business": "Equity",
                            "currency": "USD",
                            "attributes": {"calculation.interval": 10, "calculation.isPublicised": True},
                            "asOf": 1616108400},
                      headers=headers)
    id = post_response.json()['id']

    response = requests.post('http://172.21.0.3:5000//update_calcatt/'+str(id),
                             json={"calculation.interval": 'test_update', "calculation.isPublicised": False},
                             headers=headers)

    requests.delete(url + 'del_product/'+str(id))
    assert response.status_code == 200


def test_get_invalid_timestamp():
    """
    test invalid timestamp
    :return:
    """
    response = requests.get('http://172.21.0.3:5000/product/4?&timestamp=tommy')
    assert response.status_code == 404


def test_get_changes():
    """
    for this check I used object id=4 that is already saved in the database
    :return:
    """
    response = requests.get(url + 'product/4?timestamp=1626652250')
    assert response.status_code == 200
