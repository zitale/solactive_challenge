from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime as dt

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql://root:root@db/main"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True


db =SQLAlchemy(app)


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    business = db.Column(db.Enum('Equity','Fixed Income','Other'))
    currency = db.Column(db.String(3))
    attributes = db.Column(db.JSON)
    asOf = db.Column(db.BigInteger)
    def __init__(self, **kwargs):
        super(Product, self).__init__(**kwargs)

class Changes(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_item_modified = db.Column(db.Integer)
    old_attributes = db.Column(db.JSON)
    new_attributes = db.Column(db.JSON)
    timestamp = db.Column(db.BigInteger)
    __table_args__ = (db.UniqueConstraint('id_item_modified', 'timestamp'),)

@app.route('/')
def home():
    return "App Works!!!"

@app.route('/product/', methods=['POST'])
def create_product():
    try:
        request_data = request.get_json(force=True)
        # validate input please
        p = Product(name= request_data['name']
                    ,business= request_data['business']
                    ,currency= request_data['currency']
                    ,attributes=request_data['attributes']
                    ,asOf=request_data['asOf']
            )
        app.logger.info('new record created and added')
        db.session.add(p)
        app.logger.info('commit')
        db.session.commit()
        return {'id':p.id}, 201
    except Exception as e:
        return {'error': str(e)}, 400

@app.route('/del_product/<id>', methods=['DELETE'])
def delete_product(id):
    try:
        p = Product.query.get(id)
        db.session.delete(p)
        db.session.commit()
        return {},200
    except:
        return {'error':'DELETE failed'},400


@app.route('/product/<id>', methods=['GET'])
def get_product(id):

    app.logger.info('read ts and retrive p')
    ts = request.args.get('timestamp',None)

    p = Product.query.filter_by(id=id).first_or_404()
    app.logger.info('ts')

    # if ts=None return the product as it is now
    if ts is None:
        app.logger.info('ts is None return p')
        return {"id": p.id,
                "name": p.name,
                "business": p.business,
                "currency": p.currency,
                "attributes": p.attributes,
                "asOf": p.asOf}

    # check if ts has a valid format
    try:
        if str(int(ts)) == ts:
            app.logger.info('ts is ok')
    except:
        return {'error': 'ts has invalid format'}, 404

    # return result at time ts considering changes if any
    app.logger.info('return statment')
    if int(ts) < p.asOf:
        app.logger.info('ts < p.asOf')
        return {'error':'This product was not created yet'}, 404
    else:
        app.logger.info('retrieve changes')
        c = Changes.query.filter_by(id_item_modified=id).filter(Changes.timestamp <= ts).order_by(Changes.timestamp.desc()).first()
        app.logger.info('changes retrived and return')
        return {"id":p.id,
                "name": p.name,
                "business": p.business,
                "currency": p.currency,
                "attributes": p.attributes if c is None else c.new_attributes,
                "at_time":ts}, 200
    return {},404 #it should not reach this point

@app.route('/calcatt', methods=['GET'])
def get_calcatt():
    app.logger.info('read arguments')
    conds = {k: request.args.get(k) for k in ['name','business','currency','timestamp'] if request.args.get(k)}
    app.logger.info('update asOf')
    if 'timestamp' in conds.keys():
        conds['asOf'] = conds.pop('timestamp')

    app.logger.info('retrieve all p with specified arguments')
    p = Product.query
    for attr, value in conds.items():
        p = p.filter(getattr(Product, attr).like("%%%s%%" % value))
    p = p.all()

    app.logger.info('create calculation attributes for each p')
    out = {}
    for pp in p:
        out[pp.id] ={cat:catv for cat,catv in pp.attributes.items() if cat.startswith('calculation.')}
    return out

@app.route('/update_calcatt/<id>', methods=['POST'])
def update_calcatt(id):
    app.logger.info('read new_att')
    new_att = request.get_json(force=True)
    # check on new att
    app.logger.info('check on new att')
    for k,v in new_att.items():
        if k.startswith('calculation.'):
            next
        else:
            return {'error':'Attempt to modify invalid attribute, it should be a calculation one.'}, 404
    app.logger.info('retireve p')
    p = Product.query.filter_by(id=id).first_or_404('Item with id '+str(id)+' not found!')
    app.logger.info('extract old att')
    old_att = {k: p.attributes[k] for k in new_att.keys()}

    # keep track of the change
    app.logger.info('create changes record')
    c = Changes(id_item_modified = id
                ,old_attributes = old_att
                ,new_attributes = new_att
                ,timestamp = dt.now().timestamp()
                )

    app.logger.info('update p attributes in product')
    for k,v in new_att.items():
        p.attributes[k] = v

    app.logger.info('Add c')
    db.session.add(c)
    db.session.commit()
    return {},200

@app.route('/del_changes/<id>', methods=['DELETE'])
def delete_changes(id):
    app.logger.info('retrieve all element by id_item_modified')
    d = Changes.query.filter_by(id_item_modified=id).all()
    app.logger.info('for to delete by id')
    for dd in d:
        d = Changes.query.get(dd.id)
        db.session.delete(d)
        db.session.commit()
    return {},200


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')