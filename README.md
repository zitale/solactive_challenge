# SOLACTIVE CODE CHALLENGE

## Endpoints
The application has the following endpoints:

```
POST /product/
```

- returns:
    - Response Code 201 - in case of success, with a JSON body of the single attribute “id”, that can be
    used to query that product again.
    -   Response Code 400 - in case of incorrectly specified attribute values.

```
GET /product/<id>?timestamp=<ts>
```
- returns:
  - Response Code 200 – in case of success with a body containing all available attributes of the given
    product at the specified point in time.
  - Response Code 404 – in case there is no product with the given ID or the timestamp was not
    formatted correctly.
          
```
GET /calcatt
```

- arguments: 'name','business','currency','timestamp'

- returns:
  - {id: calculationa attributes} dict of the products matching the arguments

```
POST /update_calcatt/<id>
```
modifies the calculation attributes of one element also updating the changes table 

- arguments: 
  - dict with calulations attributes

- returns:
  - {id: calculationa attributes} dict of the products matching the arguments

UTILS ENDPOINTS:

```
DELETE '/del_product/<id>'
```
delete a product  
- arguments: 
  - id product in table product

```
DELETE /del_changes/<id>
```
delete all changes for a product
- arguments: 
  - id_item_modified  in table changes

## How to run the project

You can run the project simply running the following code:
```
sudo docker-compose up   
```
## Test
```
 pytest test_app.py --verbose
```

![img.png](img.png)

## Comment
It's a simple service mainting a list of product and their attibutes. I liked the challenge even if it was not clear to me how to implement the update of calculations attributes only using calculation service internal id.
With more time I would have extended the test cases and improved the quality of the code ensuring to take care of each possible case.
I would be glad to comment with you the implementation and possible further developments. 